package th.co.truemoney.paymentrequest.manager.http;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import th.co.truemoney.paymentrequest.dao.AccessToken;
import th.co.truemoney.paymentrequest.dao.BitlyShortenUrl;

/**
 * Created by teerakiat on 2/14/2017 AD.
 */

public interface BitlyService {
    @FormUrlEncoded
    @POST("oauth/access_token")
    Call<AccessToken> login(@Field("grant_type") String grantType, @Field("username") String username, @Field("password") String password);

    @GET("v3/shorten")
    Call<BitlyShortenUrl> shortenUrl(@Query("access_token")String accessToken, @Query("longUrl") String longUrl);

}
