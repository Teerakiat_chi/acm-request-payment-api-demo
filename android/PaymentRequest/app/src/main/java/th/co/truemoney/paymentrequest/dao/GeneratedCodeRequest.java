package th.co.truemoney.paymentrequest.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by teerakiat on 1/26/2017 AD.
 */

public class GeneratedCodeRequest {
    @SerializedName("payee_tmn_id")
    private String payee_tmn_id;

    @SerializedName("amount")
    private double amount;

    @SerializedName("description")
    private String description;

    @SerializedName("alias")
    private String alias;

    @SerializedName("payer_mobile_number")
    private String payer_mobile_number;

    public String getPayee_mobile_number() {
        return payee_mobile_number;
    }

    public void setPayee_mobile_number(String payee_mobile_number) {
        this.payee_mobile_number = payee_mobile_number;
    }

    @SerializedName("payee_mobile_number")
    private String payee_mobile_number;

    @SerializedName("callback_url")
    private String callback_url;

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public String getPayee_tmn_id() {
        return payee_tmn_id;
    }

    public void setPayee_tmn_id(String payee_tmn_id) {
        this.payee_tmn_id = payee_tmn_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPayer_mobile_number() {
        return payer_mobile_number;
    }

    public void setPayer_mobile_number(String payer_mobile_number) {
        this.payer_mobile_number = payer_mobile_number;
    }
}
