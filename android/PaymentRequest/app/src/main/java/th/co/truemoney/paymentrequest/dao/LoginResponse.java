
package th.co.truemoney.paymentrequest.dao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("namespace")
    @Expose
    private String namespace;
    @SerializedName("titleTh")
    @Expose
    private String titleTh;
    @SerializedName("titleEn")
    @Expose
    private String titleEn;
    @SerializedName("messageTh")
    @Expose
    private String messageTh;
    @SerializedName("messageEn")
    @Expose
    private String messageEn;
    @SerializedName("originalMessage")
    @Expose
    private String originalMessage;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getTitleTh() {
        return titleTh;
    }

    public void setTitleTh(String titleTh) {
        this.titleTh = titleTh;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getMessageTh() {
        return messageTh;
    }

    public void setMessageTh(String messageTh) {
        this.messageTh = messageTh;
    }

    public String getMessageEn() {
        return messageEn;
    }

    public void setMessageEn(String messageEn) {
        this.messageEn = messageEn;
    }

    public String getOriginalMessage() {
        return originalMessage;
    }

    public void setOriginalMessage(String originalMessage) {
        this.originalMessage = originalMessage;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
