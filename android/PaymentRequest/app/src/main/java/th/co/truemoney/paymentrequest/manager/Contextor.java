package th.co.truemoney.paymentrequest.manager;

import android.content.Context;

/**
 * Created by Teerakiat on 12/1/17 AD.
 */
public class Contextor {

    private static Contextor instance;

    public static Contextor getInstance() {
        if (instance == null)
            instance = new Contextor();
        return instance;
    }

    private Context mContext;

    private Contextor() {
        //prevent initiate Singleton template
    }

    public void init(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

}
