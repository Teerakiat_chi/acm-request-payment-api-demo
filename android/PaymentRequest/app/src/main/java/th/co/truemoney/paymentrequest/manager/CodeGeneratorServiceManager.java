package th.co.truemoney.paymentrequest.manager;

import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import th.co.truemoney.paymentrequest.manager.http.ApiCodeGeneratorService;
import th.co.truemoney.paymentrequest.util.Config;

/**
 * Created by Teerakiat on 24/01/2017.
 */
public class CodeGeneratorServiceManager {

    private static CodeGeneratorServiceManager instance;

    public static CodeGeneratorServiceManager getInstance() {
        if (instance == null)
            instance = new CodeGeneratorServiceManager();
        return instance;
    }

    private ApiCodeGeneratorService service;

    //support basic authen
    private CodeGeneratorServiceManager() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.ManageCodeUrl())
                .client(Config.getDefaultHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ApiCodeGeneratorService.class);
    }

    public ApiCodeGeneratorService getService(){
        return service;
    }
}
