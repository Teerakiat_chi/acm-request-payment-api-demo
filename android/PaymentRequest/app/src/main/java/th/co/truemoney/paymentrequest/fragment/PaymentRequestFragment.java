package th.co.truemoney.paymentrequest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.util.ExceptionCatchingInputStream;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import th.co.truemoney.paymentrequest.R;
import th.co.truemoney.paymentrequest.activity.IMainActivity;
import th.co.truemoney.paymentrequest.dao.AccessToken;
import th.co.truemoney.paymentrequest.dao.BitlyShortenUrl;
import th.co.truemoney.paymentrequest.dao.GeneratedCodeRequest;
import th.co.truemoney.paymentrequest.dao.GeneratedCodeResponse;
import th.co.truemoney.paymentrequest.dao.LoginResponse;
import th.co.truemoney.paymentrequest.manager.AppPreferences;
import th.co.truemoney.paymentrequest.manager.BitlyServiceManager;
import th.co.truemoney.paymentrequest.manager.CodeGeneratorServiceManager;
import th.co.truemoney.paymentrequest.manager.http.BitlyService;
import th.co.truemoney.paymentrequest.util.Config;


/**
 * Created by Teerakiat on 11/16/2014.
 */
public class PaymentRequestFragment extends Fragment {

    Button btnRequestBtn;
    TextView tvProfile;
    TextView tvBalance;
    ImageView ivImageProfile;

    TextInputEditText editTextDesc;
    TextInputEditText editTextTargetMobileNumber;
    TextInputEditText editTextAmount;

    RelativeLayout layout_loadingProgressBar;
    LinearLayout layout_mainForm;
    LinearLayout layout_profile;
    String tmnId;
    Double balance;
    String profileName;
    String imageUrl;

    static final String tmnIdKey = "tmnId";
    static final String balanceKey = "balance";
    static final String imageKey = "imageUrl";
    static final String profileNameKey = "profileName";

    public PaymentRequestFragment() {
        super();
    }

    public static PaymentRequestFragment newInstance(LoginResponse response) {
        PaymentRequestFragment fragment = new PaymentRequestFragment();
        Bundle args = new Bundle();
        args.putString(tmnIdKey, response.getData().getTmnId());
        args.putDouble(balanceKey, Double.parseDouble(response.getData().getCurrentBalance()));
        args.putString(imageKey, response.getData().getImageURL());
        args.putString(profileNameKey, response.getData().getFullname());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tmnId = getArguments().getString(tmnIdKey);
        profileName = getArguments().getString(profileNameKey);
        balance = getArguments().getDouble(balanceKey);
        imageUrl = getArguments().getString(imageKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payment_request, container, false);
        initInstances(rootView);

        return rootView;
    }

    private void showProgressBar() {
        layout_mainForm.setVisibility(View.GONE);
        layout_loadingProgressBar.setVisibility(View.VISIBLE);
        layout_profile.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        layout_mainForm.setVisibility(View.VISIBLE);
        layout_loadingProgressBar.setVisibility(View.GONE);
        layout_profile.setVisibility(View.GONE);
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        layout_loadingProgressBar = (RelativeLayout) rootView.findViewById(R.id.layout_progressbar);
        layout_profile = (LinearLayout) rootView.findViewById(R.id.layout_profile);
        layout_mainForm = (LinearLayout) rootView.findViewById(R.id.layout_mainForm);
        btnRequestBtn = (Button) rootView.findViewById(R.id.btn_generated_code);
        editTextDesc = (TextInputEditText) rootView.findViewById(R.id.txt_description);
        editTextTargetMobileNumber = (TextInputEditText) rootView.findViewById(R.id.txt_target_mobile_number);
        editTextAmount = (TextInputEditText) rootView.findViewById(R.id.txt_amount);
        ivImageProfile = (ImageView) rootView.findViewById(R.id.imgProfile);
        tvProfile = (TextView) rootView.findViewById(R.id.tvName);
        tvProfile.setText(profileName);

        if(!imageUrl.endsWith("null")) {
            Glide.with(getContext()).load(imageUrl)
                    .bitmapTransform(new CropCircleTransformation(getContext()))
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(ivImageProfile);

            ivImageProfile.setVisibility(View.VISIBLE);
        }

        tvBalance = (TextView) rootView.findViewById(R.id.tvBalance);
        NumberFormat formatter = new DecimalFormat("#0.00");
        tvBalance.setText(formatter.format(balance));

        btnRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressBar();

                String description = editTextDesc.getText().toString();
                double amount = Double.parseDouble(editTextAmount.getText().toString());
                String targetMobileNumber = editTextTargetMobileNumber.getText().toString();
                GeneratedCodeRequest request = new GeneratedCodeRequest();
                request.setPayee_tmn_id(tmnId);
                request.setAmount(amount);
                request.setDescription(description);
                request.setPayer_mobile_number(targetMobileNumber);
                request.setCallback_url(Config.CallbackUrl());

                final Call<GeneratedCodeResponse> lambdaRequest =
                        CodeGeneratorServiceManager.getInstance().getService().generateCode(
                                AppPreferences.getInstance().getAccessToken(), request);

                lambdaRequest.enqueue(new Callback<GeneratedCodeResponse>() {
                    @Override
                    public void onResponse(Call<GeneratedCodeResponse> call, Response<GeneratedCodeResponse> response) {
                        hideProgressBar();
                        if (response.isSuccessful()) {
                            final String code = response.body().getGenerated_code();
                            final String url = response.body().getUrl();

                            //Generate shorten Url
                            final BitlyService bitlyService = BitlyServiceManager.getInstance("c34f3a4c499699db8a639c43b8e95c6b0a5dc735", "8c6333c902e218bd643e853209ad5a6d9fd26fc2")
                                    .getService();
                            bitlyService.login("password", "teerakiatchi", "Ascend#1").enqueue(new Callback<AccessToken>() {
                                @Override
                                public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                                    bitlyService.shortenUrl(response.body().getAccess_token(), url).enqueue(new Callback<BitlyShortenUrl>() {
                                        @Override
                                        public void onResponse(Call<BitlyShortenUrl> call, Response<BitlyShortenUrl> response) {
                                            ((IMainActivity) getActivity()).showGeneratedCode(code, response.body().getData().getUrl());
                                        }

                                        @Override
                                        public void onFailure(Call<BitlyShortenUrl> call, Throwable t) {
                                            Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Call<AccessToken> call, Throwable t) {
                                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });

                        } else {
                            if(response.code() == 401){
                                //show login screen.
                                ((IMainActivity) getActivity()).tokenExpire();
                            }
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneratedCodeResponse> call, Throwable t) {
                        hideProgressBar();
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
        outState.putString(tmnIdKey, tmnId);
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
            tmnId = savedInstanceState.getString(tmnIdKey);
        }
    }
}
