package th.co.truemoney.paymentrequest.util;

import android.util.Base64;

import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class SHA256Util {

	private static final String HASH_ALGORITHM = "HmacSHA256";
	private static final String SHA256 = "SHA-256";

	public static String serializeeRFC3339Date(Date date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		String dateformat = sdf.format(date);
		int length = dateformat.length();

		String dateFirst = dateformat.substring(0,length-2);
		String dateLast = dateformat.substring(length-2,length);

		return dateFirst+":"+dateLast;
	}

	public static String base64(String msg, String keyString) {
		String digest = null;
		try {
			SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), HASH_ALGORITHM);
			Mac mac = Mac.getInstance(HASH_ALGORITHM);
			mac.init(key);

			byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
			digest = Base64.encodeToString(bytes, Base64.DEFAULT).trim();
		} catch (Exception e) {

		}
		return digest;
	}

	public static String sha256(String data) {
		try{
			MessageDigest digest = MessageDigest.getInstance(SHA256);
			byte[] hash = digest.digest(data.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}

			return hexString.toString();
		} catch(Exception ex){
			throw new RuntimeException(ex);
		}
	}
}
