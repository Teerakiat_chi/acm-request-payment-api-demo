package th.co.truemoney.paymentrequest.manager.http;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import th.co.truemoney.paymentrequest.dao.LoginRequest;
import th.co.truemoney.paymentrequest.dao.LoginResponse;

/**
 * Created by teerakiat on 1/24/2017 AD.
 */

public interface ApiLoginService {
    @POST("signin?device_os=android&app_version=1.0")
    Call<LoginResponse> login(@Body LoginRequest request);
}
