package th.co.truemoney.paymentrequest.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.zxing.common.StringUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import th.co.truemoney.paymentrequest.R;
import th.co.truemoney.paymentrequest.activity.IMainActivity;
import th.co.truemoney.paymentrequest.dao.LoginRequest;
import th.co.truemoney.paymentrequest.dao.LoginResponse;
import th.co.truemoney.paymentrequest.manager.AppPreferences;
import th.co.truemoney.paymentrequest.manager.LoginServiceManager;
import th.co.truemoney.paymentrequest.util.SHA1Util;


/**
 * Created by Teerakiat on 11/16/2014.
 */
public class LoginFragment extends Fragment {
    private Button btn_login;
    private LinearLayout layout_mainform;
    private RelativeLayout layout_progressBar;
    private TextInputEditText tv_username;
    private TextInputEditText tv_password;

    private View.OnClickListener loginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                showLoading();
                final String username = tv_username.getText().toString().toLowerCase();
                final String password = tv_password.getText().toString();
                String hashSHA1 = SHA1Util.hashSHA1(username + password);
                LoginRequest request = new LoginRequest();
                request.setUsername(username);
                request.setPassword(hashSHA1);
                request.setType("email");

                final Call<LoginResponse> loginCall = LoginServiceManager.getInstance().getService().login(request);

                loginCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        hideLoading();
                        if(response.isSuccessful()){
                            IMainActivity activity = (IMainActivity)getActivity();
                            LoginResponse loginDetail = response.body();
                            activity.loginSuccess(loginDetail);
                            AppPreferences.getInstance().saveAccessToken(loginDetail.getData().getAccessToken());
                            AppPreferences.getInstance().saveLogin(username);
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        hideLoading();
                        Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG).show();
                    }
                });

            } catch (Exception ex) {
                System.out.println("==================> " + ex.getMessage());
            }
        }
    };

    private void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showLoading(){
        hideKeyboard();
        layout_progressBar.setVisibility(View.VISIBLE);
        layout_mainform.setVisibility(View.GONE);
    }

    private void hideLoading(){
        layout_progressBar.setVisibility(View.GONE);
        layout_mainform.setVisibility(View.VISIBLE);
    }

    public LoginFragment() {
        super();
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        btn_login = (Button) rootView.findViewById(R.id.btn_login);
        btn_login.setOnClickListener(loginClickListener);

        tv_username = (TextInputEditText) rootView.findViewById(R.id.email);
        String lastLoginName = AppPreferences.getInstance().getLogin();
        tv_username.setText(lastLoginName);
        tv_password = (TextInputEditText) rootView.findViewById(R.id.password);

        layout_mainform = (LinearLayout) rootView.findViewById(R.id.layout_mainForm);
        layout_progressBar = (RelativeLayout) rootView.findViewById(R.id.layout_progressbar);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
