package th.co.truemoney.paymentrequest.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class SHA1Util {

	public static String hashSHA1(String s) {

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] bData = md.digest(s.getBytes());

			return byteArrayToHexString(bData);
		}
		catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String byteArrayToHexString(byte[] mdbytes) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}
}
