package th.co.truemoney.paymentrequest.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by teerakiat on 1/24/2017 AD.
 */

public class LoginRequest {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("type")
    private String type;
}
