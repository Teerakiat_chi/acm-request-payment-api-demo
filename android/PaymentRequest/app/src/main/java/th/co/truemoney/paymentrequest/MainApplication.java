package th.co.truemoney.paymentrequest;

import android.app.Application;

import th.co.truemoney.paymentrequest.manager.Contextor;

/**
 * Created by teerakiat on 1/22/2017 AD.
 */

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(this.getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
