package th.co.truemoney.paymentrequest.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import th.co.truemoney.paymentrequest.R;
import th.co.truemoney.paymentrequest.dao.LoginRequest;
import th.co.truemoney.paymentrequest.dao.LoginResponse;
import th.co.truemoney.paymentrequest.fragment.LoginFragment;
import th.co.truemoney.paymentrequest.fragment.PaymentRequestFragment;

public class MainActivity extends AppCompatActivity implements IMainActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initInstance();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainer, LoginFragment.newInstance())
                    .commit();
        }

    }

    private void initInstance() {
        toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.logo_wallet);

        toolbar.setSubtitle(R.string.app_subtitle);
    }

    public void showGeneratedCode(String code, String url) {
        Intent intent = new Intent(MainActivity.this, ShareCodeActivity.class);
        intent.putExtra("code", code);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    public void loginSuccess(LoginResponse response){
        //System.out.println(response.getData().getImageURL());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, PaymentRequestFragment.newInstance(response))
                .commit();
    }

    public void tokenExpire(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, LoginFragment.newInstance())
                .commit();
    }

}
