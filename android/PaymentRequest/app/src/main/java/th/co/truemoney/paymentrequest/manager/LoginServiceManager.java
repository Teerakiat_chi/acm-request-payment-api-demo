package th.co.truemoney.paymentrequest.manager;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import th.co.truemoney.paymentrequest.dao.LoginRequest;
import th.co.truemoney.paymentrequest.dao.LoginResponse;
import th.co.truemoney.paymentrequest.manager.http.ApiCodeGeneratorService;
import th.co.truemoney.paymentrequest.manager.http.ApiLoginService;
import th.co.truemoney.paymentrequest.util.Config;

/**
 * Created by teerakiat on 1/24/2017 AD.
 */

public class LoginServiceManager {

    private static LoginServiceManager instance;

    public static LoginServiceManager getInstance() {
        if (instance == null)
            instance = new LoginServiceManager();
        return instance;
    }

    private ApiLoginService service;

    private LoginServiceManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.LoginUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ApiLoginService.class);
    }

    public ApiLoginService getService(){
        return service;
    }

}
