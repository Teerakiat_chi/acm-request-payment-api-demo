package th.co.truemoney.paymentrequest.util;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import th.co.truemoney.paymentrequest.BuildConfig;

/**
 * Created by teerakiat on 1/30/2017 AD.
 */

public final class Config {

    private Config(){

    }

    public static String LoginUrl(){
        return BuildConfig.LOGIN_URL;
    }
    public static String ManageCodeUrl(){
        return BuildConfig.MANAGE_CODE_URL;
    }
    public static String CallbackUrl() {
        return BuildConfig.CALLBACK_URL;
    }
    public static OkHttpClient getDefaultHttpClient(){
        return new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }
}
