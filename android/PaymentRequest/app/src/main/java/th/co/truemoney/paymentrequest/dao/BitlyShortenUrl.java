package th.co.truemoney.paymentrequest.dao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by teerakiat on 2/14/2017 AD.
 */

public class BitlyShortenUrl {
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_txt")
    @Expose
    private String statusTxt;
    @SerializedName("data")
    @Expose
    private BitlyData data;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusTxt() {
        return statusTxt;
    }

    public void setStatusTxt(String statusTxt) {
        this.statusTxt = statusTxt;
    }

    public BitlyData getData() {
        return data;
    }

    public void setData(BitlyData data) {
        this.data = data;
    }
}
