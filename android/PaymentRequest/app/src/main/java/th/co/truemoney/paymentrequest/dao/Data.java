
package th.co.truemoney.paymentrequest.dao;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("addressList")
    @Expose
    private List<Object> addressList = null;
    @SerializedName("occupation")
    @Expose
    private Object occupation;
    @SerializedName("accessToken")
    @Expose
    private String accessToken;
    @SerializedName("profileImageStatus")
    @Expose
    private String profileImageStatus;
    @SerializedName("tmnId")
    @Expose
    private String tmnId;
    @SerializedName("lastnameEn")
    @Expose
    private Object lastnameEn;
    @SerializedName("currentBalance")
    @Expose
    private String currentBalance;
    @SerializedName("thaiId")
    @Expose
    private String thaiId;
    @SerializedName("firstnameEn")
    @Expose
    private Object firstnameEn;
    @SerializedName("kycVerifyStatus")
    @Expose
    private Boolean kycVerifyStatus;
    @SerializedName("hasPin")
    @Expose
    private Boolean hasPin;
    @SerializedName("title")
    @Expose
    private Object title;
    @SerializedName("profileType")
    @Expose
    private String profileType;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("birthdate")
    @Expose
    private Object birthdate;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("hasPassword")
    @Expose
    private Boolean hasPassword;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("imageURL")
    @Expose
    private String imageURL;

    public List<Object> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Object> addressList) {
        this.addressList = addressList;
    }

    public Object getOccupation() {
        return occupation;
    }

    public void setOccupation(Object occupation) {
        this.occupation = occupation;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getProfileImageStatus() {
        return profileImageStatus;
    }

    public void setProfileImageStatus(String profileImageStatus) {
        this.profileImageStatus = profileImageStatus;
    }

    public String getTmnId() {
        return tmnId;
    }

    public void setTmnId(String tmnId) {
        this.tmnId = tmnId;
    }

    public Object getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(Object lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getThaiId() {
        return thaiId;
    }

    public void setThaiId(String thaiId) {
        this.thaiId = thaiId;
    }

    public Object getFirstnameEn() {
        return firstnameEn;
    }

    public void setFirstnameEn(Object firstnameEn) {
        this.firstnameEn = firstnameEn;
    }

    public Boolean getKycVerifyStatus() {
        return kycVerifyStatus;
    }

    public void setKycVerifyStatus(Boolean kycVerifyStatus) {
        this.kycVerifyStatus = kycVerifyStatus;
    }

    public Boolean getHasPin() {
        return hasPin;
    }

    public void setHasPin(Boolean hasPin) {
        this.hasPin = hasPin;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Object birthdate) {
        this.birthdate = birthdate;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Boolean getHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(Boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

}
