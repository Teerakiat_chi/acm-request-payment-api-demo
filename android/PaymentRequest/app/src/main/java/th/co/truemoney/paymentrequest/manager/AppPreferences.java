package th.co.truemoney.paymentrequest.manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Teerakiat on 11/16/2014.
 */
public class AppPreferences {

    private static AppPreferences instance;
    private static final String KEY_TOKEN = "token";
    private static final String KEY_USERNAME = "loginName";
    public static AppPreferences getInstance() {
        if (instance == null)
            instance = new AppPreferences();
        return instance;
    }

    private Context mContext;
    private SharedPreferences preferences;


    private AppPreferences() {
        mContext = Contextor.getInstance().getContext();
        preferences = mContext.getSharedPreferences("app", Context.MODE_PRIVATE);
    }

    private void save(String key, String value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void saveLogin(String username){
        save(KEY_USERNAME, username);
    }

    public void saveAccessToken(String token){
        save(KEY_TOKEN, token);
    }

    public String getLogin(){
        return preferences.getString(KEY_USERNAME, "");
    }

    public String getAccessToken(){
        return preferences.getString(KEY_TOKEN, "");
    }
}
