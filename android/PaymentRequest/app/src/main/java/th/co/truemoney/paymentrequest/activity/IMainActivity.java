package th.co.truemoney.paymentrequest.activity;

import th.co.truemoney.paymentrequest.dao.LoginResponse;

/**
 * Created by teerakiat on 1/26/2017 AD.
 */

public interface IMainActivity {
    void showGeneratedCode(String code, String url);
    void loginSuccess(LoginResponse id);
    void tokenExpire();
}
