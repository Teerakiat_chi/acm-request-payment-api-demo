package th.co.truemoney.paymentrequest.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by teerakiat on 1/24/2017 AD.
 */

public class GeneratedCodeResponse {
    @SerializedName("tmn_id")
    private String tmn_id;

    @SerializedName("generated_code")
    private String generated_code;

    @SerializedName("mobile_url")
    private String mobile_url;

    @SerializedName("url")
    private String url;

    public String getTmn_id() {
        return tmn_id;
    }

    public void setTmn_id(String tmn_id) {
        this.tmn_id = tmn_id;
    }

    public String getGenerated_code() {
        return generated_code;
    }

    public void setGenerated_code(String generated_code) {
        this.generated_code = generated_code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMobile_url() {
        return mobile_url;
    }

    public void setMobile_url(String mobile_url) {
        this.mobile_url = mobile_url;
    }
}
