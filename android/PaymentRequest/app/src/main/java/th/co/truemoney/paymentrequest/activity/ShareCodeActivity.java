package th.co.truemoney.paymentrequest.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;


import th.co.truemoney.paymentrequest.R;

import static android.R.attr.width;
import static android.R.color.black;
import static android.R.color.white;
import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

/**
 * Created by teerakiat on 1/26/2017 AD.
 */

public class ShareCodeActivity extends AppCompatActivity {
    TextView txt_Code;
    Button btn_Share;
    ImageView ivQRCode;

    String code;
    String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_code);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo_wallet);
        getSupportActionBar().setTitle(getString(R.string.share_code));
        initInstance();
    }

    private void initInstance(){
        txt_Code = (TextView) findViewById(R.id.txt_generated_code);
        ivQRCode = (ImageView) findViewById(R.id.iv_qr_code);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            code = extras.getString("code");
            url = extras.getString("url");
            txt_Code.setText(code);
        }

        try {
            Bitmap bitmap = encodeAsBitmap(url);
            ivQRCode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        btn_Share = (Button) findViewById(R.id.btn_share_code);

        btn_Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);

                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_code));
                shareIntent.putExtra(Intent.EXTRA_TEXT, url + " **Share via True Money Wallet Transfer @Beta Android App");

                startActivity(Intent.createChooser(shareIntent, getString(R.string.share_code)));
            }
        });
    }


    private  Bitmap encodeAsBitmap(String str) throws WriterException {

        try
        {
            com.google.zxing.Writer writer = new QRCodeWriter();
            // String finaldata = Uri.encode(data, "utf-8");
            int width = 250;
            int height = 250;
            BitMatrix bm = writer
                    .encode(str, BarcodeFormat.QR_CODE, width, height);
            Bitmap ImageBitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);

            for (int i = 0; i < width; i++) {// width
                for (int j = 0; j < height; j++) {// height
                    ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK
                            : Color.WHITE);
                }
            }
            return ImageBitmap;
        } catch (Exception iae) {
            Toast.makeText(ShareCodeActivity.this, iae.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("code", code);
        outState.putString("url", url);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        code = savedInstanceState.getString("code");
        url = savedInstanceState.getString("url");
    }


}
