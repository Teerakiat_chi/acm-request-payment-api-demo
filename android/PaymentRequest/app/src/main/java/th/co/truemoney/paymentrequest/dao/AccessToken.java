package th.co.truemoney.paymentrequest.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by teerakiat on 2/14/2017 AD.
 */

public class AccessToken {
    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    @SerializedName("access_token")
    private String access_token;
}
