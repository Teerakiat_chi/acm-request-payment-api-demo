package th.co.truemoney.paymentrequest.manager;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import th.co.truemoney.paymentrequest.BuildConfig;
import th.co.truemoney.paymentrequest.manager.http.BitlyService;
import th.co.truemoney.paymentrequest.util.Config;

/**
 * Created by Teerakiat on 11/16/2014.
 */
public class BitlyServiceManager {

    private static BitlyServiceManager instance;

    public static BitlyServiceManager getInstance(String username, String password) {
        if (instance == null)
            instance = new BitlyServiceManager(username, password);
        return instance;
    }

    private BitlyService bitlyService;

    private BitlyServiceManager(String username, String password) {
        String authToken = Credentials.basic(username, password);
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);

        OkHttpClient defaultHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BITLY_URL)
                .client(defaultHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        bitlyService = retrofit.create(BitlyService.class);
    }

    public BitlyService getService(){
        return bitlyService;
    }
}
