package th.co.truemoney.paymentrequest.manager.http;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import th.co.truemoney.paymentrequest.dao.GeneratedCodeRequest;
import th.co.truemoney.paymentrequest.dao.GeneratedCodeResponse;

/**
 * Created by teerakiat on 1/24/2017 AD.
 */

public interface ApiCodeGeneratorService {

    @POST("payments/codes")
    Call<GeneratedCodeResponse> generateCode(@Header("Authorization") String token, @Body GeneratedCodeRequest request);

}
