'use strict'

var https = require('https');

exports.handler =  (event, context, callback) => {
    console.log(JSON.stringify(event))
    
    var token = event.authorizationToken;
    // Call oauth provider, crack jwt token, etc.
    // In this example, the token is treated as the status for simplicity.

    if(token) {
        https.get('https://alpha-api-ewm.tmn-dev.com/api/v1/profile/' + token, (res) => {
            const statusCode = res.statusCode;
            var rawData = '';
            res.on('data', (chunk) => rawData += chunk);
            res.on('end', () => {
                try {
                    var parsedData = JSON.parse(rawData);
                    console.log(parsedData);
                    const data = {
                        statusCode,
                        profile: parsedData.data,
                        event
                    };
                    createResponse(callback, data);
                } catch (e) {
                    console.log(e.message);
                }
            });
        });
    } else {
        createResponse(callback, null);
    }
};

var createResponse = function(callback, data) {
    switch (data.statusCode) {
        case 200:
            callback(null, generatePolicy(data.profile, 'Allow', data.event.methodArn));
            break;
        case 500:
            callback("Unauthorized");   // Return a 401 Unauthorized response
            break;
        default:
            callback("Error: Invalid token"); 
    }
};

var generatePolicy = function(profile, effect, resource) {
    var authResponse = {};
    
    authResponse.principalId = profile.tmnId;
    if (effect && resource) {
        var policyDocument = {};
        policyDocument.Version = '2012-10-17'; // default version
        policyDocument.Statement = [];
        var statementOne = {};
        statementOne.Action = 'execute-api:Invoke'; // default action
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }

    // Can optionally return a context object of your choosing.
    authResponse.context = { }
    authResponse.context.mobileNumber = profile.mobileNumber
    
    console.log(`authResponse: ${JSON.stringify(authResponse)}`)
    return authResponse;
};

/*exports.handler(
    {
        authorizationToken: '970ddcc9-b8fc-4843-90c3-1ff93d555747',
        methodArn: 'methodArn'
    },
    {},
    function(err, res) {
        if(err) {
            console.log(err);
        } else {
            console.log(res);
        }
    }
);*/
