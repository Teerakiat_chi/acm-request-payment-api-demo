var getPaymentDetail = require('./getPaymentDetail')

const expire_time = 60 * 60  //in seconds

module.exports = (event, redis, data) => {
    return new Promise((resolve, reject) => {

        // get paid input
        var updatingPaid = {}
        event.params.payers.forEach( e => {
            if(event.requestContext.authorizer.mobileNumber == e.payer_mobile_number) {
                updatingPaid = { payer_mobile_number: e.payer_mobile_number, paid: e.paid }
                return
            }
        })

        if( Object.keys(updatingPaid).length !== 0 ) {
            data.value.payers.forEach((e, i) => {
                if(event.requestContext.authorizer.mobileNumber == e.payer_mobile_number) {
                    data.value.payers[i] = updatingPaid
                    return
                }
            })
        }

        redis.set(data.key, JSON.stringify(data.value), 'EX', expire_time,
            (err, results) => {
                if (err) {
                    reject('fail to set value into cache')
                } else {
                    resolve(data.value)
                }
            })
    })
}
