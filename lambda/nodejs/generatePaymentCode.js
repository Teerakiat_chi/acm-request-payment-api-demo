var uuid = require('node-uuid')
var getPaymentDetail = require('./getPaymentDetail')

const expire_time = 60 * 60  //in seconds

//api: generate payment request code
//input
// {
//     tmn_id:string,
//     alias:string
//     amount:number,
//     description:string
// }

// response
// {
//      generated_code:string,
//      status {
//          code:int, //200: success, 500: error 
//          description:string
//      }
// }
module.exports = (event, context, redis) => {
    return new Promise((resolve, reject) => {
        if(!event.params.amount){
            reject('amount must be provided!')
            return;
        }
        if(!event.params.payers){
            reject('payer mobile phone must be provided!')
            return;
        }
        if (event.params.alias) {
            generated_code = event.params.alias
        } else {
            generated_code = uuid.v4()
        }

        console.log(`uuid: ${generated_code}`)

        //check first
        getPaymentDetail(redis, { match: `*:${generated_code}` }).then((results) => {
            
            if (results.length == 0) {
                //redis: store a key as tmn_id:generated_code
                //redis: value as json object contains following field
                // { alias, amount, description }

                if( Object.prototype.toString.call( event.params.payers ) !== '[object Array]' ) {
                   event.params.payers = [event.params.payers]
                }

                const payers = event.params.payers.map((payer_mobile_number) => { return { payer_mobile_number, paid: false }})

                const value = JSON.stringify({
                    alias: event.params.alias,
                    amount: event.params.amount,
                    description: event.params.descriptions,
                    payers: payers,
                    payee_mobile_number: event.requestContext.authorizer.mobileNumber,
                    payee_tmn_id: event.requestContext.authorizer.principalId,
                    callback_url: event.params.callback_url
                })
                redis.set(`${global.paymentCode}|${event.requestContext.authorizer.principalId}:${generated_code}`, value, 'EX', expire_time,
                    function (err, results) {
                        if (err) {
                            reject('fail to set value into cache')
                        } else {
                            resolve(generated_code)
                        }
                    })
                    
            } else {
                reject(`${generated_code}: already get generated`)
            }
        })
    })
}
