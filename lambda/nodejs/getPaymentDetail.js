

//api: get payment detail by code
//input
// {
//     code:string
// }

// response
// {
//      alias
//      amount
//      description
// }
module.exports = (redis, func) => {
    return new Promise((resolve, reject) => {
        var stream = redis.scanStream({
            // only returns keys following the pattern passing by caller
            match: func.match,
            // returns approximately 100 elements per call
            count: 10
        })

        var items = [];
        stream.on('data', function (resultKeys) {
            // `resultKeys` is an array of strings representing key names
            for (var i = 0; i < resultKeys.length; i++) {
                items.push({ key: resultKeys[i] });
            }
        })

        stream.on('end', function () {
            var pipeline = redis.pipeline();
            for (item of items) {
                pipeline.get(item.key)
            }

            pipeline.exec().then((results) => {
                //results return array of [err, value]
                for (i = 0; i < items.length; i++) {
                    const error = results[i][0]
                    if (error) {
                        console.error(error)
                    } else {
                        items[i].value = results[i][1]
                    }
                }
                resolve(items)
            })

        })
    })

}
