

//api: delete payment code
//input
// {
//     code:string
// }

// response
// {
//      status
// }
module.exports = (code, redis) => {
    return new Promise((resolve, reject) => {
        if (!code || code === '') {
            reject('payment code must not be empty!')
            return;
        }
        console.log(`deleting ${code}`)
        redis.keys(code).then( (keys) => {
            var pipeline = redis.pipeline();
            keys.forEach( (key)=>{
                pipeline.del(key)
            })
            pipeline.exec().then( (result) => {
                console.log(`result: ${JSON.stringify(result)}`)
                //return [[null, 1]]
                if(result.length == 0){
                    reject({
                            statusCode: 404,
                            body: JSON.stringify({errorMessage: "Not Found"})
                        })
                }else{
                    resolve({code:'success', message:'success'})
                }
            })
        })
    })
}