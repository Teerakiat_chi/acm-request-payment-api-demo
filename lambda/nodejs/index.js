'use strict'
var Redis = require('ioredis')
global.paymentCode = 'PAYMENT_CODE'

const port = 6379;
//const host = '127.0.0.1'; //'alpha-tapf-redis.tmn-dev.com'
const host = 'alpha-tapf-redis.tmn-dev.com'
const options = {
    reconnectOnError: function (err) {
        var targetError = 'READONLY';
        if (err.message.slice(0, targetError.length) === targetError) {
            // Only reconnect when the error starts with "READONLY"
            // This feature is useful when using Amazon ElastiCache. 
            // Once failover happens, Amazon ElastiCache will switch the master we currently connected with to a slave, 
            // leading to the following writes fails with the error READONLY. Using reconnectOnError, 
            // we can force the connection to reconnect on this error in order to connect to the new master.
            return true; // or `return 1;`
        }
    }
}
var redis = new Redis(port, host, options)
redis.on('error', function (err) {
    console.error(err)
})
redis.on('connect', function () {
    console.log(`redis connected ${host}`)
})
const generatePayment = require('./generatePaymentCode')
const getPaymentDetail = require('./getPaymentDetail')
const deletePaymentCode = require('./deletePaymentCode')
const updatePaymentDetail = require('./updatePaymentDetail')

exports.generateCodeHandler = (event, context) => {
    event.params = JSON.parse(event.body)
    console.log(`entering ${context.functionName}`)
    console.log(`receieving request: ${event.requestContext.authorizer.principalId}`)
    
    context.callbackWaitsForEmptyEventLoop = false

    generatePayment(event, context, redis)
    .then((code) => {
        const response = {
            payee_tmn_id: event.requestContext.authorizer.principalId,
            generated_code: code,
            url: `${event.params.callback_url}/${code}`,
            mobile_url: `ascendmoney:wallet?code=${code}`
        }
        context.succeed(
            {
                statusCode: 201,
                body: JSON.stringify(response)
            }
        )
    }).catch((err) => {
        context.succeed(
            {
                statusCode: 400,
                body: JSON.stringify({errorMessage: err})
            }
        )
    })

};

exports.getPaymentDetailByCode = (event, context) => {
    console.log(`entering ${context.functionName}`)
    console.log(`receieving request: ${event.requestContext.authorizer.principalId}|${event.requestContext.authorizer.mobileNumber}`)
    
    context.callbackWaitsForEmptyEventLoop = false
    
    const func = {
        match: `${global.paymentCode}|*:${event.pathParameters.code}`
    }

    getPaymentDetail(redis, func)
    .then((results) => {
        return getFirstPayment(results)
    }).then((data) => {
        return checkPermission(event, data)
    }).then((data) => {
        context.succeed(
            {
                statusCode: 200,
                body: JSON.stringify(data.value)
            }
        )
    }).catch((err) => {
        context.succeed(err)
    })
}

exports.getPaymentDetailById = (event, context) => {
    console.log(`entering ${context.functionName}`)
    console.log(`receieving request: ${event.requestContext.authorizer.principalId}`)
    
    context.callbackWaitsForEmptyEventLoop = false

    const func = {
        match: `${global.paymentCode}|${event.requestContext.authorizer.principalId}:*`
    }

    getPaymentDetail(redis, func).then((results) => {
        let response = results.map((data) => { 
            return {
                key: data.key,
                value: JSON.parse(data.value)
            };
        })
        context.succeed(
            {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        )
    })
}

exports.deletePaymentCode = (event, context) => {
    console.log(`entering ${context.functionName}`)
    console.log(`receieving request: ${event.requestContext.authorizer.principalId}|${event.requestContext.authorizer.mobileNumber}`)

    context.callbackWaitsForEmptyEventLoop = false
    let keyCode = `${global.paymentCode}|*:${event.pathParameters.code}`
    getPaymentDetail(redis, { match: keyCode })
    .then((results) => {
        return getFirstPayment(results)
    }).then((data) => {
        return checkPermission(event, data)
    }).then(() => {
        return deletePaymentCode(keyCode, redis)
    }).then((result) => {
        context.succeed(
            {
                statusCode: 200,
                body: JSON.stringify(result)
            }
        )
    }).catch((err) => {
        context.succeed(err)
    })
}

exports.updatePaymentDetailByCode = (event, context) => {
    event.params = JSON.parse(event.body)
    console.log(`entering ${context.functionName}`)
    console.log(`receieving request: ${event.requestContext.authorizer.principalId}|${event.requestContext.authorizer.mobileNumber}`)
    
    context.callbackWaitsForEmptyEventLoop = false

    const func = {
        match: `${global.paymentCode}|*:${event.pathParameters.code}`
    }

    getPaymentDetail(redis, func)
    .then((results) => {
        return getFirstPayment(results)
    }).then((data) => {
        return checkPermission(event, data)
    }).then((data) => {
        return updatePaymentDetail(event, redis, data)
    }).then((value) => {
        context.succeed(
            {
                statusCode: 200,
                body: JSON.stringify(value)
            }
        )
    }).catch((err) => {
        context.succeed(err)
    })
}

function getFirstPayment(results) {
    return new Promise((resolve, reject) => {
        if(results && results.length >0){
            const data = results[0];
            resolve(data)
        } else {
            reject({
                statusCode: 404,
                body: JSON.stringify({errorMessage: "Not Found"})
            })
        }
    })
}

function checkPermission(event, data) {
    return new Promise((resolve, reject) => {
        // console.log(data)
        data.value = JSON.parse(data.value);
        if(data.value.payee_tmn_id == event.requestContext.authorizer.principalId) {
            resolve(data)
        } else {
            data.value.payers.forEach((item) => {
                if (item.payer_mobile_number == event.requestContext.authorizer.mobileNumber) {
                    resolve(data)
                }
            })
        }
        reject({
            statusCode: 403,
            body: JSON.stringify({errorMessage: "Forbidden"})
        })
    })
}

//for test
//  exports.deletePaymentCode(
//      {
//         requestContext: {
//              authorizer: {
//                 mobileNumber: "0811734526"
//              }
//         },
//         pathParameters: {
//             code: "aaa4"
//         }
//      },
//      {
//          functionName: 'deletePaymentCode',
//          succeed: (response) => console.log(response)
//      })

//for test
// exports.getPaymentDetailById(
//      {
//          requestContext: {
//              authorizer: {
//                 principalId: "tmn.10000079641"
//              }
//          }
//      },
//      {
//          functionName: 'getPaymentDetailById',
//          succeed: (response) => console.log(response)
//      });

//for test
//  exports.getPaymentDetailByCode(
//      {
//          requestContext: {
//              authorizer: {
//                 principalId: "tmn.10000079641"
//              }
//          },
//          pathParameters: {
//              code: "aaa3"
//          }
//      },
//      {
//          functionName: 'getPaymentDetailByCode',
//          succeed: (response) => console.log(response)
//      });

//  exports.getPaymentDetailByCode(
//      {
//          requestContext: {
//              authorizer: {
//                 mobileNumber: "0863361709"
//              }
//          },
//          pathParameters: {
//              code: "aaa3"
//          }
//      },
//      {
//          functionName: 'getPaymentDetailByCode',
//          succeed: (response) => console.log(response)
//      });

// for test
// exports.generateCodeHandler(
//      {
//          requestContext: {
//              authorizer: {
//                 principalId: "tmn.10000079641",
//                 mobileNumber: "0811734526"
//              }
//          },
//          body: "{ \n\"payers\": \"0863361709\",\n\"amount\": 55, \n\"alias\": \"aaa3\", \n\"callback_url\":\"http://localhost:8000/wallet#/request-p2p\" \n}",
//      },
//      {
//         functionName: 'generateCode',
//         succeed: (response) => console.log(response)
//      }
//  )

//  exports.generateCodeHandler(
//      {
//          requestContext: {
//              authorizer: {
//                 principalId: "tmn.10000079641",
//                 mobileNumber: "0811734526"
//              }
//          },
//          body: "{ \n\"payers\": [\"0863361709\", \"0811734526\"],\n\"amount\": 55, \n\"alias\": \"aaa4\", \n\"callback_url\":\"http://localhost:8000/wallet#/request-p2p\" \n}",
//      },
//      {
//         functionName: 'generateCode',
//         succeed: (response) => console.log(response)
//      }
//  )


// exports.updatePaymentDetailByCode(
//     {
//          requestContext: {
//              authorizer: {
//                 mobileNumber: "0863361709"
//              }
//          },
//          pathParameters: {
//              code: "aaa4"
//          },
//          body: "{\"payers\":[{\"payer_mobile_number\":\"0863361709\",\"paid\":true}]}",
//      },
//      {
//         functionName: 'updatePaymentDetailByCode',
//         succeed: (response) => console.log(response)
//      }
// )